<?php

// use Illuminate\Http\request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'TodosController@index');

// Route::resource('todo','TodosController');
Route::resource('api/items','ItemsController');

Route::get('/posts', function () {
    return view('post');
});




// Route::get('/posts', function () {
//   // $title = "A blog Title";
//   // $content = "lorem ipsum donor !";
//    return view('post',['title' => 'blog title whoohooo']);
//     // return view('post',Compact('title','content'));
// });


// Route::get('home', function () {
//     return ('you are not old enough hahaha !!!!!');
// });
//
// Route::get('fountain-of-youth',['middleware'=>'ageCheck',function() {
//     return ('welcome to Fountain of Youth 100 years added to your age  Whoohoo!!!');
// }]);



// Route::get('/request-test',function(request $request){
//     return $request->all();
// });

// Route::any('/josef', function () {
//     return ('welcome  this your first webpage in laravel !!');
// });
//
//
// Route::match(['get','post'],'/josef', function () {
//     return ('welcome  this your first webpage !');
// });
