<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Blog title - @yield('title')</title>
  </head>
  <body>
    <h1>Title of the webpage</h1>

    
  @section('sidebar')
    area of sidebar

  @endsection
  @section('sidebar')
              This is the master sidebar.
          @show
  <main>
    @yield('content')
  </main>

  </body>
</html>
